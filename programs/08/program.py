#!/usr/bin/env python3

from sys import argv
from dolfinx import (io, fem, mesh)
from dolfinx.io import gmshio
from scipy import constants
import ufl
import numpy as np
from mpi4py import MPI
from slepc4py import SLEPc
from petsc4py.PETSc import ScalarType


target_freq = 1200e6
r_copper = 1.72e-8


def interpolate(V, f):
    u, v = ufl.TrialFunction(V), ufl.TestFunction(V)
    a_p = ufl.inner(u, v) * ufl.dx
    L_p = ufl.inner(f, v) * ufl.dx
    return fem.petsc.LinearProblem(a_p, L_p).solve()


def grad_rz(scal):
    vr = scal.dx(0)
    vz = scal.dx(1)
    return ufl.as_vector((vr, vz))


def curl_rz(vec):
    return vec[0].dx(1) - vec[1].dx(0)


def curl_phi(scal, r):
    vr = -scal.dx(1)
    vz = (1 / r) * (r * scal).dx(0)
    return ufl.as_vector((vr, vz))


def E2H(f, omega):
    return 1j * curl_rz(f) / omega / constants.mu_0


msh, _, boundaries = gmshio.read_from_msh(f"{argv[1]}.msh", MPI.COMM_WORLD, 0, gdim=2)

# with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}.xdmf", "r") as xdmf:
#     msh = xdmf.read_mesh(name="Grid")
mdim = msh.topology.dim - 1
# msh.topology.create_connectivity(mdim, mdim + 1)
# with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}_line.xdmf", "r") as xdmf:
#     boundaries = xdmf.read_meshtags(msh, name="Grid")

x_points = msh.geometry.x[msh.geometry.x[:, 1] == 0][:, 0]
z_min, z_max = np.min(x_points), np.max(x_points)
z_length = z_max - z_min

N1curl = ufl.FiniteElement("Nedelec 1st kind H(curl)", msh.ufl_cell(), 1)
H1 = ufl.FiniteElement("Lagrange", msh.ufl_cell(), 1)
V = fem.FunctionSpace(msh, ufl.MixedElement(N1curl, H1))
Vproj2d = fem.FunctionSpace(msh, ufl.VectorElement(H1, dim=2))
Vproj1d = fem.FunctionSpace(msh, H1)

e, p = ufl.TrialFunctions(V)
psi, q = ufl.TestFunctions(V)

z, r = ufl.SpatialCoordinate(msh)

normal_vec = ufl.FacetNormal(msh)
tangent_vec = ufl.as_vector((-normal_vec[1], normal_vec[0]))

a = r * ufl.inner(curl_rz(e), curl_rz(psi)) * ufl.dx + \
        r * ufl.inner(grad_rz(p), psi) * ufl.dx
a += r * ufl.inner(e, grad_rz(q)) * ufl.dx
b = r * ufl.inner(e, psi) * ufl.dx

bc_facets_all = mesh.exterior_facet_indices(msh.topology)
bc_facets_axis = boundaries.indices[boundaries.values == 1]
bc_facets_symm = boundaries.indices[boundaries.values == 2]
bc_facets_vacuum = boundaries.indices[boundaries.values == 3]
bc_facets_metal = np.setdiff1d(bc_facets_all, np.hstack([bc_facets_symm, bc_facets_vacuum, bc_facets_axis]))
bc_facets_bc = np.setdiff1d(bc_facets_all, bc_facets_axis)
dofs_bc = fem.locate_dofs_topological(V, mdim, bc_facets_bc)
dofs_axis_proj2d = fem.locate_dofs_topological(Vproj2d, mdim, bc_facets_axis)
coords_axis_proj2d = (Vproj2d.tabulate_dof_coordinates()[dofs_axis_proj2d])
dofs_proj2d = fem.locate_dofs_topological(Vproj2d, mdim, bc_facets_bc)
coords_proj2d = (Vproj2d.tabulate_dof_coordinates()[dofs_proj2d])
dofs_axis_proj1d = fem.locate_dofs_topological(Vproj1d, mdim, bc_facets_axis)
coords_axis_proj1d = (Vproj1d.tabulate_dof_coordinates()[dofs_axis_proj1d])
dofs_proj1d = fem.locate_dofs_topological(Vproj1d, mdim, bc_facets_bc)
coords_proj1d = (Vproj1d.tabulate_dof_coordinates()[dofs_proj1d])


ds = ufl.Measure('ds', domain=msh,
                 subdomain_data=mesh.meshtags(msh, mdim,
                                              bc_facets_metal, 1),
                 subdomain_id=1)
ds_axis = ufl.Measure('ds', domain=msh,
                      subdomain_data=mesh.meshtags(msh, mdim,
                                                   bc_facets_axis, 1),
                      subdomain_id=1)

u_bc = fem.Function(V)
u_bc.x.array[:] = ScalarType(0)
bc = fem.dirichletbc(u_bc, dofs_bc)

A = fem.petsc.create_matrix(fem.form(a))
fem.petsc.assemble_matrix(A, fem.form(a), bcs=[bc], diagonal=0.0)
A.assemble()
B = fem.petsc.create_matrix(fem.form(b))
fem.petsc.assemble_matrix(B, fem.form(b), bcs=[bc])
B.assemble()

eps = SLEPc.EPS().create(MPI.COMM_WORLD)
eps.setOperators(A, B)
eps.setType(SLEPc.EPS.Type.KRYLOVSCHUR)
st = eps.getST()
st.setType(SLEPc.ST.Type.SINVERT)
eps.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_MAGNITUDE)
eps.setTarget(0.5 * 2 * np.pi * target_freq /
              constants.speed_of_light)
eps.setDimensions(10)
eps.solve()

vals = [(i, eps.getEigenvalue(i)) for i in range(eps.getConverged())]
vals.sort(key=lambda x: x[1].real)

j = 0
E = fem.Function(V)
with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}_sol.xdmf", "w") as xdmf:
    xdmf.write_mesh(msh)
    for i, _ in vals:
        E1, E2 = E.split()
        k_sqrd = eps.getEigenpair(i, E.vector).real
        omega = np.sqrt(k_sqrd / constants.epsilon_0 /
                        constants.mu_0)
        f = omega / (2 * np.pi)
        penalty = np.abs(fem.assemble_scalar(
                fem.form(E2 * ufl.dx)))
        if not np.isclose(penalty, 0):
            continue
        print(f"frequency [MHz]: {f/1e6:.3f}")
        max_index = np.argmax(E1.x.array)
        phase = np.arctan2(E1.x.array[max_index].imag, E1.x.array[max_index].real)
        E1.x.array[:] = E1.x.array * np.exp(1j * -phase)
        H = E2H(E1, omega)
        W = 2 * np.abs(fem.assemble_scalar(
                fem.form((constants.epsilon_0 * ufl.real(E1) ** 2 +
                          constants.mu_0 * ufl.real(H) ** 2) / 2 *
                         2 * np.pi * r * ufl.dx)))
        E1.x.array[:] = E1.x.array / np.sqrt(W)
        delta = np.sqrt(2 * r_copper / omega / constants.mu_0)
        Q = np.abs(
                2 / delta *
                fem.assemble_scalar(fem.form(H ** 2 *
                                             2 * np.pi * r * ufl.dx)) /
                fem.assemble_scalar(fem.form((H * tangent_vec) ** 2 *
                                             2 * np.pi * r * ds)))
        print(f"Q: {Q:.3e}")
        P = omega / Q
        print(f"P [MW]: {P/1e6:.3e}")
        rsh = np.abs(2 * fem.assemble_scalar(fem.form(ufl.dot(tangent_vec, E1) * ds_axis)) ** 2 / P / z_length)
        print(f"rsh [MOhm/m]: {rsh/1e6:.3e}")
        beta = 2 * (2 * z_length) * f / (constants.speed_of_light)
        print(f"beta: {beta:.3f}")
        rsh_eff = np.abs(fem.assemble_scalar(fem.form(
		(ufl.dot(tangent_vec, E1) *
		 ufl.exp(1j * (z_max - z) * omega / beta / constants.speed_of_light - 0.5j * np.pi)) *
		ds_axis)) ** 2 / P / z_length)
        print(f"rsh_eff [MOhm/m]: {rsh_eff/1e6:.3e}")
        print(f"T: {np.sqrt(rsh_eff/rsh):.3f}")
        func = interpolate(Vproj2d, E1)
        max_index = np.argmax(func.x.array)
        phase = np.arctan2(func.x.array[max_index].imag, func.x.array[max_index].real)
        data = func.x.array[dofs_axis_proj2d * 2].reshape(-1, 1)
        field = np.concatenate((coords_axis_proj2d, data), axis=1)
        np.savetxt(f"E_axis_{j+1:03d}.csv",
                   np.real(field),
                   delimiter=",",
                   fmt="%1.5e",
                   header="x,y,z,data",
                   comments="")
        data = func.x.array[dofs_proj2d * 2].reshape(-1, 1)
        field = np.concatenate((coords_proj2d, data), axis=1)
        np.savetxt(f"E_{j+1:03d}.csv",
                   np.real(field),
                   delimiter=",",
                   fmt="%1.5e",
                   header="x,y,z,data",
                   comments="")
        func.name = f"E_{j+1:03d}"
        xdmf.write_function(func)
        func = interpolate(Vproj1d, H)
        data = func.x.array[dofs_axis_proj1d].reshape(-1, 1)
        field = np.concatenate((coords_axis_proj1d, np.imag(data)), axis=1)
        np.savetxt(f"H_axis_{j+1:03d}.csv",
                   np.real(field),
                   delimiter=",",
                   fmt="%1.5e",
                   header="x,y,z,data",
                   comments="")
        data = func.x.array[dofs_proj1d].reshape(-1, 1)
        field = np.concatenate((coords_proj1d, np.imag(data)), axis=1)
        np.savetxt(f"H_{j+1:03d}.csv",
                   np.real(field),
                   delimiter=",",
                   fmt="%1.5e",
                   header="x,y,z,data",
                   comments="")
        func.name = f"H_{j+1:03d}"
        xdmf.write_function(func)
        j += 1
