#!/usr/bin/env python3

from sys import argv
import gmsh
import numpy as np
from pathlib import Path
from yaml import load, CLoader


dir = Path(__file__).parent

with open(dir / f"{argv[1]}.yaml", "r") as yamlfile:
	data = load(yamlfile, Loader=CLoader)

gmsh.initialize()

gmsh.merge(f"{argv[1]}.step")

gmsh.model.get_bounding_box(-1, -1)

gmsh.model.occ.dilate(gmsh.model.get_entities(dim=-1), 0, 0, 0, 1e-3, 1e-3, 1e-3)
gmsh.model.occ.mesh.set_size(gmsh.model.get_entities(dim=-1), data["mesh"]["size"])
# gmsh.model.mesh.set_order(1)
gmsh.model.occ.synchronize()

gmsh.model.get_bounding_box(-1, -1)

# remove volume
gmsh.model.remove_entities(gmsh.model.get_entities(dim=3))


def points_get_coords(points):
	return np.array([gmsh.model.get_value(0, point, []) for point in points])


def lines_get_points(lines):
	return np.unique(np.hstack([gmsh.model.get_adjacencies(1, line)[1] for line in lines]))


def surfaces_get_lines(surfaces):
	return np.unique(np.hstack([gmsh.model.get_adjacencies(2, surface)[1] for surface in surfaces]))


def surface_get_coords(surface):
	return points_get_coords(lines_get_points(surfaces_get_lines([surface])))

# remove surfaces
gmsh.model.remove_entities([(dim, tag) for dim, tag in gmsh.model.get_entities(dim=2)
                            if np.any(~np.isclose(surface_get_coords(tag)[:, 2], 0))])

# remove lines
gmsh.model.remove_entities([(dim, tag) for dim, tag in gmsh.model.get_entities(dim=1)
                            if gmsh.model.get_adjacencies(1, tag)[0].size == 0])

# remove points
gmsh.model.remove_entities([(dim, tag) for dim, tag in gmsh.model.get_entities(dim=0)
                            if gmsh.model.get_adjacencies(0, tag)[0].size == 0])

gmsh.model.geo.synchronize()

lines = surfaces_get_lines([tag for dim, tag in gmsh.model.get_entities(dim=2)])

gmsh.model.add_physical_group(2, [tag for dim, tag in gmsh.model.get_entities(dim=2)],
                              tag=1, name="surface")
gmsh.model.add_physical_group(1, [line for line in lines
                                  if np.all(np.isclose(points_get_coords(lines_get_points([line]))[:, 1], 0))],
                              tag=1, name="horizontal axis")
gmsh.model.add_physical_group(1, [line for line in lines
                                  if np.all(np.isclose(points_get_coords(lines_get_points([line]))[:, 0], 0))],
                              tag=2, name="left vertical")
gmsh.model.add_physical_group(1, [line for line in lines if (points_get_coords(lines_get_points([line]))[0, 0] ==
							     points_get_coords(lines_get_points([line]))[1, 0] and
							     points_get_coords(lines_get_points([line]))[0, 0] > 0)],
			      tag=3, name="right vertical")

gmsh.model.mesh.generate(2)
gmsh.write(f"{argv[1]}.msh")

# gmsh.fltk.run()
