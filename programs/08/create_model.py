#!/usr/bin/env freecadcmd

from sys import argv
from pathlib import Path
from yaml import load, CLoader

dir = Path(__file__).parent

doc = FreeCAD.openDocument(str(dir / f"{argv[1]}.FCStd"))
with open(dir / f"{argv[1]}.yaml", "r") as yamlfile:
	data = load(yamlfile, Loader=CLoader)
sheet = doc.findObjects(Label="params")[0]
obj = doc.findObjects(Label="object")[0]

for key, val in data["geometry"].items():
	sheet.set(sheet.getCellFromAlias(key), str(val))

sheet.recompute()
doc.recompute()
obj.Shape.exportStep(str(dir / f"{argv[1]}.step"))
