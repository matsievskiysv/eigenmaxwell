#!/usr/bin/env python3

from sys import argv
from dolfinx import (io, fem)
from dolfinx.io import gmshio
import ufl
from mpi4py import MPI
from slepc4py import SLEPc
from petsc4py.PETSc import ScalarType


def interpolate(V, f):
    u, v = ufl.TrialFunction(V), ufl.TestFunction(V)
    a_p = ufl.inner(u, v) * ufl.dx
    L_p = ufl.inner(f, v) * ufl.dx
    return fem.petsc.LinearProblem(a_p, L_p).solve()


def curl_rz(vec):
    return vec[0].dx(1) - vec[1].dx(0)


def curl_phi(scal, r):
    vr = -scal.dx(1)
    vz = (1 / r) * (r * scal).dx(0)
    return ufl.as_vector((vr, vz))


msh, _, boundaries = gmshio.read_from_msh(f"{argv[1]}.msh", MPI.COMM_WORLD, 0, gdim=2)

# with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}.xdmf", "r") as xdmf:
#     msh = xdmf.read_mesh(name="Grid")
# msh.topology.create_connectivity(msh.topology.dim-1, msh.topology.dim)
# with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}_line.xdmf", "r") as xdmf:
#     boundaries = xdmf.read_meshtags(msh, name="Grid")


N1curl = ufl.FiniteElement("Nedelec 1st kind H(curl)", msh.ufl_cell(), 1)
V_rz = fem.FunctionSpace(msh, N1curl)
H1 = ufl.FiniteElement("Lagrange", msh.ufl_cell(), 1)
Vproj = fem.FunctionSpace(msh, ufl.VectorElement(H1, dim=2))

_, r = ufl.SpatialCoordinate(msh)

E_rz, v_rz = ufl.TrialFunction(V_rz), ufl.TestFunction(V_rz)

a = r * ufl.inner(curl_rz(E_rz), curl_rz(v_rz)) * ufl.dx
b = r * ufl.inner(E_rz, v_rz) * ufl.dx

bc_facets = boundaries.indices[boundaries.values == 1]
bc_dofs = fem.locate_dofs_topological(V_rz, msh.topology.dim-1, bc_facets)

u_bc = fem.Function(V_rz)
u_bc.x.array[bc_dofs] = ScalarType(0)
bc = fem.dirichletbc(u_bc, bc_dofs)

A = fem.petsc.create_matrix(fem.form(a))
fem.petsc.assemble_matrix(A, fem.form(a), bcs=[bc])
A.assemble()
B = fem.petsc.create_matrix(fem.form(b))
fem.petsc.assemble_matrix(B, fem.form(b), bcs=[bc], diagonal=0.0)
B.assemble()

eps = SLEPc.EPS().create(MPI.COMM_WORLD)
eps.setOperators(A, B)
eps.setType(SLEPc.EPS.Type.KRYLOVSCHUR)
st = eps.getST()
st.setType(SLEPc.ST.Type.SINVERT)
eps.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_MAGNITUDE)
eps.setTarget(12)
eps.setDimensions(30)
eps.solve()

vals = [(i, eps.getEigenvalue(i)) for i in range(eps.getConverged())]
vals.sort(key=lambda x: x[1].real)

j = 0
E = fem.Function(V_rz)
with io.XDMFFile(MPI.COMM_WORLD, f"{argv[1]}_sol.xdmf", "w") as xdmf:
    xdmf.write_mesh(msh)
    for i, _ in vals:
        print(f"{eps.getEigenpair(i, E.vector):.4f}")
        func = interpolate(Vproj, E)
        func.name = f"E_{j+1:03d}"
        xdmf.write_function(func)
        j += 1
