#!/usr/bin/env pvbatch

from paraview.simple import *
from re import sub
from os import environ

part = environ["PART"]

paraview.simple._DisableFirstRenderCameraReset()

renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.ResetCamera(False)

outxdmf = XDMFReader(registrationName='input', FileNames=[f'{part}_sol.xdmf'])
Hide(outxdmf, renderView1)

extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=outxdmf)
glyph1 = Glyph(registrationName='Glyph1', Input=extractBlock1, GlyphType='Arrow')

for name in [x for x in outxdmf.GridStatus if x != "mesh"]:
    name_vtk = sub(r"[ \-.,]", "", name)
    if outxdmf.PointData[f"real_{name}"] is None:
        continue
    rng = outxdmf.PointData[f"real_{name}"].GetRange()
    print(rng)
    m = max([abs(x) for x in rng])
    extractBlock1.Selectors = [f'/Root/{name_vtk}']
    extractBlock1Display = Show(extractBlock1, renderView1, 'UnstructuredGridRepresentation')
    extractBlock1Display.SetRepresentationType('Outline')
    glyph1.OrientationArray = ['POINTS', f'real_{name}']
    glyph1.ScaleArray = ['POINTS', f'real_{name}']
    glyph1.GlyphMode = 'Uniform Spatial Distribution (Surface Sampling)'
    glyph1.ScaleFactor = float(environ.get("SCALE_FACTOR", "1"))
    glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')
    glyph1Display.SetRepresentationType('Surface')
    ColorBy(glyph1Display, ('POINTS', f'real_{name}', 'Magnitude'))
    renderView1.Update()
    Render()
    renderView1.ResetCamera(False)
    glyph1Display.SetScalarBarVisibility(renderView1, True)
    glyph1Display.RescaleTransferFunctionToDataRange(False, True)
    renderView1.Update()
    cam = renderView1.GetActiveCamera()
    cam.Dolly(float(environ.get("DOLLY", "1")))
    Render()
    SaveScreenshot(f"{name}.png", renderView1, ImageResolution=(800, 600), CompressionLevel=9)
    glyph1Display.SetScalarBarVisibility(renderView1, False)
