#!/usr/bin/env python3

from dolfinx import (io, fem, mesh)
import ufl
import numpy as np
from mpi4py import MPI
from slepc4py import SLEPc
from petsc4py.PETSc import ScalarType


def interpolate(V, f):
    u, v = ufl.TrialFunction(V), ufl.TestFunction(V)
    a_p = ufl.inner(u, v) * ufl.dx
    L_p = ufl.inner(f, v) * ufl.dx
    return fem.petsc.LinearProblem(a_p, L_p).solve()


msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((0, 0), (2, 1)),
                            n=(40, 40),
                            cell_type=mesh.CellType.triangle)

msh.topology.create_connectivity(msh.topology.dim-1, msh.topology.dim)

N1curl = ufl.FiniteElement("Nedelec 1st kind H(curl)", msh.ufl_cell(), 1)
H1 = ufl.FiniteElement("Lagrange", msh.ufl_cell(), 1)
V = fem.FunctionSpace(msh, ufl.MixedElement(N1curl, H1))
Vproj = fem.FunctionSpace(msh, ufl.VectorElement(H1, dim=2))

e, p = ufl.TrialFunctions(V)
psi, q = ufl.TestFunctions(V)

a = ufl.inner(ufl.curl(e), ufl.curl(psi)) * ufl.dx + ufl.inner(ufl.grad(p), psi) * ufl.dx
a += ufl.inner(e, ufl.grad(q)) * ufl.dx
b = ufl.inner(e, psi) * ufl.dx

bc_facets = mesh.exterior_facet_indices(msh.topology)
bc_dofs = fem.locate_dofs_topological(V, msh.topology.dim-1, bc_facets)

u_bc = fem.Function(V)
u_bc.x.array[:] = ScalarType(0)
bc = fem.dirichletbc(u_bc, bc_dofs)

A = fem.petsc.create_matrix(fem.form(a))
fem.petsc.assemble_matrix(A, fem.form(a), bcs=[bc])
A.assemble()
B = fem.petsc.create_matrix(fem.form(b))
fem.petsc.assemble_matrix(B, fem.form(b), bcs=[bc], diagonal=0.0)
B.assemble()

eps = SLEPc.EPS().create(MPI.COMM_WORLD)
eps.setOperators(A, B)
eps.setType(SLEPc.EPS.Type.KRYLOVSCHUR)
st = eps.getST()
st.setType(SLEPc.ST.Type.SINVERT)
eps.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_MAGNITUDE)
eps.setTarget(10)
eps.setDimensions(30)
eps.solve()

vals = [(i, eps.getEigenvalue(i)) for i in range(eps.getConverged())]
vals.sort(key=lambda x: x[1].real)

j = 0
E = fem.Function(V)
with io.XDMFFile(MPI.COMM_WORLD, "out.xdmf", "w") as xdmf:
    xdmf.write_mesh(msh)
    for i, _ in vals:
        E1, E2 = E.split()
        print(f"eigenvalue: {eps.getEigenpair(i, E.vector).real:.12f};\t"
              f"int(psi): {np.abs(fem.assemble_scalar(fem.form(E2 * ufl.dx))):.12e}")
        func = interpolate(Vproj, E1)
        func.name = f"E_{j+1:03d}"
        xdmf.write_function(func)
        j += 1
