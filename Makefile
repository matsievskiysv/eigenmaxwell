# Config file
MKRC ?= latexmkrc
INDENT_SETTINGS ?= indent.yaml

# Source .tex file
SOURCE ?= main

# LaTeX compiler output .pdf file
TARGET ?= $(SOURCE)

# Ghostscript-based pdf postprocessing
include compress.mk

# LaTeX version:
# -pdf		= pdflatex
# -pdfdvi	= pdflatex with dvi
# -pdfps	= pdflatex with ps
# -pdfxe	= xelatex with dvi (faster than -xelatex)
# -xelatex	= xelatex without dvi
# -pdflua	= lualatex with dvi  (faster than -lualatex)
# -lualatex	= lualatex without dvi
BACKEND ?= -pdflua

LATEXFLAGS ?= -halt-on-error -file-line-error -shell-escape
LATEXMKFLAGS ?= -silent
BIBERFLAGS ?= --fixinits
REGEXDIRS ?= . # distclean dirs
TIMERON ?= # show CPU usage
TIKZFILE ?= # .tikz file for tikz rule
PRINTFILE ?= # .pdf file to print

# Makefile options
MAKEFLAGS := -s
.DEFAULT_GOAL := all
.NOTPARALLEL:

export NOTESON
export LATEXFLAGS
export BIBERFLAGS
export REGEXDIRS
export TIMERON
export TIKZFILE
export PRINTFILE

##! компиляция всех файлов
all: main

define compile
	latexmk -norc -r $(MKRC) $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) $(SOURCE)
endef

%.fmt: %.tex
	etex -ini -halt-on-error -file-line-error \
	-shell-escape -jobname=$(TARGET) \
	"&latex" mylatexformat.ltx """$^"""

##! компиляция документа
main: TARGET=main
main: SOURCE=main
main:
	$(compile)

##! предкомпиляция документа
preformat: $(SOURCE).fmt main

##! компиляция tikz графики
tikz: SOURCE=images/cache/tikz
tikz: BACKEND=-pdflua # некоторые библиотеки работают только с lualatex
tikz: TARGET=$(basename $(notdir $(TIKZFILE)))
tikz:
	$(compile)

##! печать файла на 2 страницы на 1
print: SOURCE=images/cache/print
print: TARGET=$(basename $(notdir $(PRINTFILE)))_print
print:
	$(compile)

##! очистка от временных файлов цели TARGET
clean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -c $(SOURCE)

##! полная очистка от временных файлов цели TARGET
distclean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -C $(SOURCE)

##! очистка проекта от временных файлов
clean:
	"$(MAKE)" SOURCE=main TARGET=main clean-target

##! полная очистка проекта от временных файлов
distclean:
	"$(MAKE)" SOURCE=main TARGET=main distclean-target

INDENT_FILES ?= $(wildcard *.tex)
##! форматирование файлов *.tex
indent:
	@$(foreach file, $(INDENT_FILES),\
	latexindent -l=$(INDENT_SETTINGS) -s -w $(file);)

##! форматирование файлов *.tex с разбиением длинных строк
indent-wrap: INDENT_SETTINGS+=-m
indent-wrap: indent

# https://gist.github.com/klmr/575726c7e05d8780505a
##! это сообщение
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^##! / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^##! //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n##! /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')

.PHONY: all main preformat tikz print clean-target distclean-target clean distclean help
